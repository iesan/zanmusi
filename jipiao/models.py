from django.db import models

# Create your models here.


class users(models.Model):
   name = models.CharField(max_length=30)
   phone = models.CharField(max_length=11)
   userID = models.CharField(max_length=18)
   def __str__(self):
      return '姓名:{} 电话:{} 身份证:{}'.format(self.name,self.phone,self.userID)


class friends(models.Model):
   uID = models.CharField(max_length=18)
   name = models.CharField(max_length=30)
   phone = models.CharField(max_length=11)
   thisID = models.CharField(max_length=18)
   def __str__(self):
      return '代购人:{} 姓名:{} 电话:{} 身份证:{}'.format(self.uID,self.name,self.phone,self.thisID)


# def 