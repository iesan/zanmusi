from django.shortcuts import render
import time
from django.http import StreamingHttpResponse
from django.utils.timezone import now

# Create your views here.


def stream_generator():
    while True:
        # 发送事件数据
        # yield 'event: date\ndata: %s\n\n' % str(now())

        # 发送数据
        yield u'data: %s\n\n' % str(now())
        time.sleep(2)
def eventsource(request):
    response = StreamingHttpResponse(stream_generator(), content_type="text/event-stream")
    response['Cache-Control'] = 'no-cache'
    return response





#1.当手机端发送聊天请求时调用;
def app(req):
  return render(req,'b.html')
def app_push(req):
    fileno = req.GET('fileno')
    data = req.GET('data')
    # yield 1




#1.客服登陆的管理页面;
def web(req):
   a = req.GET.get('a',0)
   b = req.GET.get('b',0)
   c = req.GET.get('c',0)
   d = req.GET.get('d',0)
   e = req.GET.get('e',0)
   return render(req,'a.html')